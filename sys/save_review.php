<?php

$product_id = isset($_POST['product_id']) ? $_POST['product_id'] : null;
$user_id = isset($_POST['user_id']) ? $_POST['user_id'] : null;
$rating = isset($_POST['rating']) ? $_POST['rating'] : 0;
$comment = isset($_POST['comment']) ? $_POST['comment'] : null;

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://www.raffael.nagel.link/marketplace/sys/insert_review.php",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "product_id=".$product_id."&user_id=".$user_id."&rating=".$rating."&comment=".$comment,
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/x-www-form-urlencoded",
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
