<?php

  require 'db.php';

  $db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

  $first_name = isset($_POST['first_name']) ? $_POST['first_name'] : null;
  $last_name = isset($_POST['last_name']) ? $_POST['last_name'] : null;
  $email = isset($_POST['email']) ? $_POST['email'] : null;
  $address = isset($_POST['address']) ? $_POST['address'] : '';
  $phone_home = isset($_POST['phone_home']) ? $_POST['phone_home'] : '';
  $phone_mobile = isset($_POST['phone_mobile']) ? $_POST['phone_mobile'] : '';
  echo json_encode($_POST);
  if (!$first_name || !$last_name || !$email) {
    echo json_encode(['result' => 'error']);
    exit;
  }
  $query = "INSERT INTO USER (first_name,last_name,email,address,phone_home,phone_mobile)
    VALUES (
      '".htmlspecialchars($first_name)."','".
      htmlspecialchars($last_name)."','".
      htmlspecialchars($email)."','".
      htmlspecialchars($address)."','".
      htmlspecialchars($phone_home)."','".
      htmlspecialchars($phone_mobile).
    "')";
  $result = mysqli_query($db,$query);

  $db->close();
  if ($result) {
    echo json_encode(['result' => 'success']);
  } else {
    echo json_encode(['result' => 'error']);
  }
?>
