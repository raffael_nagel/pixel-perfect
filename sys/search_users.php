<?php

  require 'db.php';

  $db = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

  $select = 'SELECT * FROM USER WHERE ';
  if (isset($_POST["name"]) && $_POST["name"] !== '') {
    $name = htmlspecialchars($_POST['name']);
    $select .= ' (first_name like "%'.$name.'%" OR last_name like "%'.$name.'%") AND ';
  }
  if (isset($_POST['email']) && $_POST["email"] !== '') {
    $email = htmlspecialchars($_POST['email']);
    $select .= ' email like "%'.$email.'%" AND ';
  }
  if (isset($_POST['phone']) && $_POST["phone"] !== '') {
    $phone = htmlspecialchars($_POST['phone']);
    $select .= ' (phone_home = "'.$phone.'" OR phone_mobile = "'.$phone.'") AND ';
  }
  $select = $select . ' true;';
  $query = $db->query($select);
  $result = array();
  if ($query->num_rows > 0) {
    $i = 0;
    while($row = $query->fetch_assoc()) {
        $result[$i] = $row;
        $i++;
    }
  }
  $db->close();
  echo json_encode($result);
?>
