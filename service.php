<?php
  session_start();
  require 'sys/load_product.php';
  require 'sys/log_view.php';
  $product = loadProduct($_GET['p']);
  $product_id = $product['id'];
  $product['description'] = $product['description'] ?: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi sagittis, sem quis lacinia faucibus, orci ipsum gravida tortor, vel interdum mi sapien ut justo. ';
  $user_id = isset($_SESSION['logged_user']) ? $_SESSION['logged_user'] : 2;
  log_view($user_id, $product_id);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="RaffaelNagel" />
    <meta name="description" content="SJSU Project">
    <!-- Document title -->
    <title>Pixel Perfect | professional photographers</title>
    <!-- Stylesheets & Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="css/plugins.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- Template color -->
    <link href="css/color-variations/blue.css" rel="stylesheet" type="text/css" media="screen">
</head>

<body>
    <!-- Wrapper -->
    <div id="wrapper">
        <!-- Header -->
        <header id="header" class="header-transparent header-fullwidth header-plain dark">
            <div id="header-wrap">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="index.html" class="logo" data-dark-logo="images/pixelperfectlogo.png">
                            <img src="images/pixelperfectlogo.png" alt="Pixel Perfect">
                        </a>
                    </div>
                    <!--End: Logo-->

                    <!--Top Search Form-->
                    <div id="top-search">
                        <form action="search-results-page.html" method="get">
                            <input type="text" name="q" class="form-control" value="" placeholder="Start typing & press  &quot;Enter&quot;">
                        </form>
                    </div>
                    <!--end: Top Search Form-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li><a href="index.html#">Home</a></li>
                                    <li><a href="index.html#about">About</a></li>
                                    <li><a href="index.html#blog">News</a></li>
                                    <li><a href="index.html#services">Services</a></li>
                                    <li><a href="index.html#clients">Clients</a></li>
                                    <li><a href="index.html#contact">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
        <!-- end: Header -->

        <!-- Page title -->
        <section id="page-title" style="background-color:#7391ac; min-height:700px;">
            <div id="particles-js" class="particles" style="background: none"></div>

            <div class="container">
                <div class="col-md-7">
                    <div class="page-title">
                        <div data-animation="fadeIn" class="text-light">
                            <h1><?php echo $product['title'];?></h1></div>


                        <div class="portfolio-share m-b-40" data-animation="fadeIn" data-animation-delay="1800">
                            <h4 class="text-light">Share this service</h4>
                            <div class="align-center">

                                <a class="btn btn-xs btn-light btn-slide" href="#">
                                    <i class="fa fa-facebook"></i>
                                    <span>Facebook</span>
                                </a>
                                <a class="btn btn-xs btn-light btn-slide" href="#" data-width="100">
                                    <i class="fa fa-twitter"></i>
                                    <span>Twitter</span>
                                </a>
                                <a class="btn btn-xs btn-light btn-slide" href="#" data-width="118">
                                    <i class="fa fa-instagram"></i>
                                    <span>Instagram</span>
                                </a>
                                <a class="btn btn-xs btn-light btn-slide" href="mailto:#" data-width="80">
                                    <i class="fa fa-envelope"></i>
                                    <span>Mail</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="project-description" data-animation="fadeIn" data-animation-delay="2200">
                    </div>
                </div>
            </div>
        </section>

        <section class="p-b-10">
        	<div class="container">
        		<div class="row p-b-20">
        			<div class="col-md-6">
        				<div class="heading heading text-left">
        					<h2><?php echo $product['title'];?></h2>
        					<span class="lead"><?php echo $product['description'];?></span>
        				</div>
        			</div>
        			<div class="col-md-6 m-t-60">
                <img src="<?php echo "images/".$product['image']?>" class="img-responsive img-rounded" alt="">
        			</div>
        		</div>
        	</div>
        </section>

        <section id="top" class="background-grey ">
          <div class="container">
            <div class="row">
              <!-- content -->
              <div class="content col-md-12">
                <div class="comments" id="comments">
                  <div class="comment_number">
                    Comments
                  </div>

                  <div class="comment-list" id="comments-div">

                  </div>
                </div>
              </div>
            </div>

          <div class="respond-form" id="respond">
              <div class="respond-comment">
                  Leave a <span>Comment</span></div>
              <form class="form-gray-fields" id="form-comment">
                <input type="hidden" id="product_id" name="product_id" value="<?php echo $product_id?>">
                <input type="hidden" id="user_id" name="user_id" value="<?php echo isset($_SESSION['logged_user']) ? $_SESSION['logged_user'] : 2 ?>">
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                        <label class="upper" for="rating">Rating</label>
                        <select class="form-control" name="rating" id="rating">
                          <option value="1" default>1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                          <option value="10">10</option>
                        </select>
                    </div>
                  </div>
                </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="upper" for="comment">Your comment</label>
                              <textarea class="form-control required" name="comment" rows="5" placeholder="Enter comment" id="comment" aria-required="true"></textarea>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group text-center">
                              <button class="btn" type="button" onclick="saveComment()">Submit Comment</button>
                          </div>
                      </div>
                  </div>
              </form>
          </div>
            </div>
        </section>


        <!-- Footer -->
        <footer id="footer" class="footer-light">

            <div class="copyright-content">
                <div class="container">
                    <div class="copyright-text text-center">&copy; 2017 Raffael Nagel
                    </div>
                </div>
            </div>
        </footer>
        <!-- end: Footer -->

    </div>
    <!-- end: Wrapper -->

    <!-- Go to top button -->
    <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>

    <!--Plugins-->
    <script src="js/jquery.js"></script>
    <script src="js/plugins.js"></script>

    <!--Template functions-->
    <script src="js/functions.js"></script>

    <!-- Partical Js files  -->
    <script src="js/plugins/components/particles.js" type="text/javascript"></script>
    <script src="js/plugins/components/particles-animation.js" type="text/javascript"></script>
    <!-- end: Partical Js files  -->

    <!--Scripts-->
    <script type="text/javascript">
      function listComments() {
        $.ajax({
          type: 'POST',
          url: 'sys/load_reviews.php',
          data: {'product_id': <?php echo $product_id?>},
          success: function (result) {
            console.log(result);
            result = $.parseJSON(result);
            var comments = $('#comments-div');
            comments.empty();
            result.forEach(function(row) {
              var avatar = 'images/user'+row['user_id']%5+'.png';
              comments.append(
                '<div class="comment p-20" id="comment-'+row['id']+'">'+
                '  <div class="image"><img alt="" src="'+avatar+'" class="avatar"></div>'+
                '    <div class="text">'+
                '      <h5 class="name">'+row['first_name']+ ' '+ row['last_name']+ '</h5>' +
                '      <span class="comment_date">Rating '+row['rating']+'/10</span>'+
                '      <div class="text_holder">'+
                '        <p>'+(row['comment'] == null ? '' :row['comment']) +'</p>'+
                '      </div>'+
                '    </div>'+
                '  </div>'+
                "</div>"
              );
            });
          }
        });
      }
      listComments();
      function saveComment() {
        $.ajax({
          type: 'POST',
          url: 'sys/save_review.php',
          data: $('#form-comment').serialize(),
          success: function (result) {
            console.log(result);
            $('#form-comment')[0].reset();
            listComments();
          },
          error: function (error) {
            console.log(error);
          }
        });
      }
      </script>

</body>

</html>
