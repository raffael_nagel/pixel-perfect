<?php
  $last_seen = json_decode($_COOKIE['pixel_perfect_viewed_services'], true);
  $last_seen = array_slice($last_seen, 0, 5);
  $service_image = [
    'artwork_capture' => 'images/portfolio/service9.jpg',
    'portraits' => 'images/portfolio/service1.jpg',
    'scanning' => 'images/portfolio/service3.jpg',
    'brand_campaign' => 'images/portfolio/service5.jpg',
    'photoshop_design' => 'images/portfolio/service2.jpg',
    'wedding_and_events' => 'images/portfolio/service6.jpg',
    'marketing' => 'images/portfolio/service7.jpg',
    'classes' => 'images/portfolio/service8.jpg',
    'commercial' => 'images/portfolio/service4.jpg',
    'freelance' => 'images/portfolio/service10.jpg',
  ];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="RaffaelNagel" />
    <meta name="description" content="SJSU Project">
    <!-- Document title -->
    <title>Pixel Perfect | professional photographers</title>
    <!-- Stylesheets & Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="css/plugins.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- Template color -->
    <link href="css/color-variations/blue.css" rel="stylesheet" type="text/css" media="screen">
</head>

<body>
    <!-- Wrapper -->
    <div id="wrapper">
        <!-- Header -->
        <header id="header" class="header-transparent header-fullwidth header-plain dark">
            <div id="header-wrap">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="index.html" class="logo" data-dark-logo="images/pixelperfectlogo.png">
                            <img src="images/pixelperfectlogo.png" alt="Pixel Perfect">
                        </a>
                    </div>
                    <!--End: Logo-->

                    <!--Top Search Form-->
                    <div id="top-search">
                        <form action="search-results-page.html" method="get">
                            <input type="text" name="q" class="form-control" value="" placeholder="Start typing & press  &quot;Enter&quot;">
                        </form>
                    </div>
                    <!--end: Top Search Form-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li><a href="index.html#">Home</a></li>
                                    <li><a href="index.html#about">About</a></li>
                                    <li><a href="index.html#blog">News</a></li>
                                    <li><a href="index.html#services">Services</a></li>
                                    <li><a href="index.html#clients">Clients</a></li>
                                    <li><a href="index.html#contact">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
        <!-- end: Header -->

        <!-- Page title -->
        <section id="page-title" class="page-title-center" data-parallax-image="images/services.jpeg">
        <div class="container">
        <div class="page-title">
                    <h1>Last 5 visited Services</h1>
                </div>
            </div>
        </section>
        <!-- end: Page title -->

        <!-- Content -->
        <section id="page-content">
            <div class="container">
              <!-- Portfolio -->
              <div id="portfolio" class="grid-layout portfolio-3-columns" data-margin="0">
              <?php
                foreach($last_seen as $service) {
                  echo '<div class="portfolio-item img-zoom pf-illustrations pf-media pf-icons pf-Media">
                      <div class="portfolio-item-wrap">
                          <div class="portfolio-image">
                              <a href="service.php?s='.$service.'"><img src="'.$service_image[$service].'" alt=""></a>
                          </div>
                          <div class="portfolio-description">
                              <a href="service.php?s='.$service.'">
                                  <h3>'.ucwords(str_replace('_','',$service)).'</h3>
                              </a>
                          </div>
                      </div>
                  </div>';
                }
              ?>


                </div>
                <!-- end: Portfolio -->
            </div>
        </section>
        <!-- end: Content -->

        <!-- Footer -->
        <footer id="footer" class="footer-light">

            <div class="copyright-content">
                <div class="container">
                    <div class="copyright-text text-center">&copy; 2017 Raffael Nagel
                    </div>
                </div>
            </div>
        </footer>
        <!-- end: Footer -->

    </div>
    <!-- end: Wrapper -->

    <!-- Go to top button -->
    <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>

    <!--Plugins-->
    <script src="js/jquery.js"></script>
    <script src="js/plugins.js"></script>

    <!--Template functions-->
    <script src="js/functions.js"></script>

    <!-- Partical Js files  -->
    <script src="js/plugins/components/particles.js" type="text/javascript"></script>
    <script src="js/plugins/components/particles-animation.js" type="text/javascript"></script>
    <!-- end: Partical Js files  -->

</body>

</html>
