<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="RaffaelNagel" />
    <meta name="description" content="SJSU Project">
    <!-- Document title -->
    <title>Pixel Perfect | professional photographers</title>
    <!-- Stylesheets & Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,800,700,600|Montserrat:400,500,600,700|Raleway:100,300,600,700,800" rel="stylesheet" type="text/css" />
    <link href="css/plugins.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">

    <!-- Template color -->
    <link href="css/color-variations/blue.css" rel="stylesheet" type="text/css" media="screen">
</head>

<body>
    <!-- Wrapper -->
    <div id="wrapper">
        <!-- Header -->
        <header id="header" class="header-transparent header-fullwidth header-plain dark">
            <div id="header-wrap">
                <div class="container">
                    <!--Logo-->
                    <div id="logo">
                        <a href="index.html" class="logo" data-dark-logo="images/pixelperfectlogo.png">
                            <img src="images/pixelperfectlogo.png" alt="Pixel Perfect">
                        </a>
                    </div>
                    <!--End: Logo-->

                    <!--Top Search Form-->
                    <div id="top-search">
                        <form action="search-results-page.html" method="get">
                            <input type="text" name="q" class="form-control" value="" placeholder="Start typing & press  &quot;Enter&quot;">
                        </form>
                    </div>
                    <!--end: Top Search Form-->

                    <!--Navigation Resposnive Trigger-->
                    <div id="mainMenu-trigger">
                        <button class="lines-button x"> <span class="lines"></span> </button>
                    </div>
                    <!--end: Navigation Resposnive Trigger-->

                    <!--Navigation-->
                    <div id="mainMenu" class="light">
                        <div class="container">
                            <nav>
                                <ul>
                                    <li><a href="index.html#">Home</a></li>
                                    <li><a href="index.html#about">About</a></li>
                                    <li><a href="index.html#blog">News</a></li>
                                    <li><a href="index.html#services">Services</a></li>
                                    <li><a href="index.html#clients">Clients</a></li>
                                    <li><a href="index.html#contact">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!--end: Navigation-->
                </div>
            </div>
        </header>
        <!-- end: Header -->
        <!-- Page title -->
                <section id="page-title" data-parallax-image="images/users-bg.jpg">
        	<div class="container">
        		<div class="page-title">
        			<h1>Users</h1>
        			<span>User page (cURL)</span>
        		</div>
        	</div>
        </section>
        <!-- end: Page title -->

        <!-- Section -->
        <section>
          <div class="container">
            <div class="row">
              <div class="col-md-8 center no-padding">
                    <div class="hr-title hr-long center"><abbr>PixelPerfect User's List</abbr> </div>
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Mobile</th>
                          </tr>
                        </thead>
                        <tbody id="user-table">
                        </tbody>
                      </table>
                    </div>
                    <!--END: Striped Table-->

              </div>
            </div>
          </div>
        </section>


        <!-- Section -->
        <section>
          <div class="container">
            <div class="row">
              <div class="col-md-8 center no-padding">
                    <div class="hr-title hr-long center"><abbr>CompanyB User's List</abbr> </div>
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Address</th>
                            <th>Phone</th>
                            <th>Mobile</th>
                          </tr>
                        </thead>
                        <tbody id="user-table2">
                        </tbody>
                      </table>
                    </div>
                    <!--END: Striped Table-->

              </div>
            </div>
          </div>
        </section>

        <!-- Footer -->
        <footer id="footer" class="footer-light">

            <div class="copyright-content">
                <div class="container">
                    <div class="copyright-text text-center">&copy; 2017 Raffael Nagel
                    </div>
                </div>
            </div>
        </footer>
        <!-- end: Footer -->

    </div>
    <!-- end: Wrapper -->

    <!-- Go to top button -->
    <a id="goToTop"><i class="fa fa-angle-up top-icon"></i><i class="fa fa-angle-up"></i></a>

    <!--Plugins-->
    <script src="js/jquery.js"></script>
    <script src="js/plugins.js"></script>

    <!--Template functions-->
    <script src="js/functions.js"></script>

    <!-- Partical Js files  -->
    <script src="js/plugins/components/particles.js" type="text/javascript"></script>
    <script src="js/plugins/components/particles-animation.js" type="text/javascript"></script>
    <!-- end: Partical Js files  -->
    <!--Scripts-->
    <script type="text/javascript">
      function listUsersFromCURL(url, tableID){
        $.ajax({
          type: 'POST',
          url: 'curl_call.php',
          data: {'url': url},
          success: function (result) {
            console.log(result);
            result = $.parseJSON(result);
            var table = $(tableID);
            table.empty();
            result.forEach(function(row) {
              table.append(
                "<tr>"+
                "<td>"+row['id']+"</td>"+
                "<td>"+row['first_name']+"</td>"+
                "<td>"+row['last_name']+"</td>"+
                "<td>"+row['email']+"</td>"+
                "<td>"+row['address']+"</td>"+
                "<td>"+row['phone_home']+"</td>"+
                "<td>"+row['phone_mobile']+"</td>"+
                "</tr>"
              );
            });
          }
        });
      }
      listUsersFromCURL('http://www.raffael.nagel.link/pixel-perfect/sys/search_users.php','#user-table');
      listUsersFromCURL('http://www.raffael.nagel.link/pixel-perfect/sys/search_users.php','#user-table2');

    </script>

</body>

</html>
