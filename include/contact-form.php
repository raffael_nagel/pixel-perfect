<?php
header('Expires: ' . gmdate('r', 0));
header('Content-type: application/json');

$name = isset($_POST["name"]) ? $_POST["name"] : null;
$email = $_POST["email"];
$phone = isset($_POST["phone"]) ? $_POST["phone"] : null;

if ($name !== null) {
	$file = "contacts.txt";
	$content = $name." - ".$phone." - ".$email."\n";
	$result = file_put_contents($file, $content, FILE_APPEND | LOCK_EX);
	$response = array ('response'=>'success', 'name' => $name, 'phone' => $phone);
	echo json_encode($response);
} else {
	$response = array ('response'=>'error');
	echo json_encode($response);
}
?>
